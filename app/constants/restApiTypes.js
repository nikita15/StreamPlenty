/**
 * Created by innopolis on 28.10.16.
 */
"use strict";
import React, {Component} from 'react';

export class Constants extends Component {
    //Api
    static HTTP = 'http://';
    static HTTPS = 'https://';
    static DOMEN = 'sp-mobile-api.azurewebsites.net/api/';
    static API_VERSION = 'v1/';

    //Services
    static USER_SERVICE = 'user/';
    static VIDEO_SERVICE = 'videos/';
    
    //Token
    static TOKEN = '676a4c0f-2dae-4079-96b1-73417485d74a';

    static POST_USER_REGISTRATION_CONFIRM_REQUEST = Constants.HTTP + Constants.DOMEN + Constants.USER_SERVICE + 'confirm';
    static GET_NEXT_VIDEO_REQUEST = Constants.HTTP + Constants.DOMEN + Constants.VIDEO_SERVICE + 'next';
}