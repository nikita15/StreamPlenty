import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import VideoFrame from '../components/videoFrame';
import { Actions } from 'react-native-router-flux';
import {bindActionCreators} from 'redux';
import {ActionCreators} from '../actions';
import { connect } from 'react-redux';

//import Health from '../services/health';

class Launch extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    console.log('s');
    this.props.getNextVideo(this.props.accessToken);
  }

  render() {
    return (
      <VideoFrame style={styles.container} uri={this.props.newStreamUrl} type={"m3u8"}/>
    );
  }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(store) {
    return {
        newStreamUrl: store.videoReducer.currentStreamUrl,
        accessToken: store.userReducer.accessToken,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Launch);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 20
  }
})