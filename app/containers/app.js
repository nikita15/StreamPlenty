import React, {Component} from 'react';
import {Navigator} from 'react-native';
import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import {Provider, connect} from 'react-redux';
import promises from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import {Actions, Router, Scene} from 'react-native-router-flux';
import reducer from '../reducers';
import Launch from './launch';
import createLogger from "redux-logger";

const loggerMiddleware = createLogger({predicate:(getState, action)=>__DEV__ });
//const createStoreWithMiddleware = applyMiddleware(thunk, loggerMiddleware)(createStore);
//const reducer = combineReducers(reducers);
//const store = createStoreWithMiddleware(reducer);
const ConnectedRouter = connect()(Router);

function configureStore(initialState) {
    const enhancer = compose(
        applyMiddleware(
            thunk,
            loggerMiddleware,
            promises()
        )
    );
    return createStore(reducer, initialState, enhancer);

}

const store = configureStore({});

const Scenes = Actions.create(
  <Scene key='root'>
    <Scene key='launch' component={Launch} hideNavBar={true} />
  </Scene>
)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter scenes={Scenes}/>
      </Provider>
    );
  }
}