import {combineReducers} from 'redux';
import * as videoReducer from './videoReducer';
import navReducer from './navReducer';
import * as userReducer from './userReducer';

export default combineReducers(Object.assign(
  videoReducer,
  userReducer
));
