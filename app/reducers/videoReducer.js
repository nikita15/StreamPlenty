import * as types from '../constants/actionTypes';

const initialState = {
  count: 0
};

export function videoReducer(state = {
  currentStreamUrl: ''
}, action) {
  switch (action.type) {
    case types.GET_NEXT_VIDEO_FULFILLED:  
      return {
        ...state,
        currentStreamUrl: action.payload
      };
    case types.DECREMENT:
      return {
        ...state,
        count: state.count - 1
      };
    default:
      return {...state }
  }
}