import {Constants as types} from "../constants/restApiTypes";
//import XMLParser from "react-xml-parser";
//import DOMParser from "xmldom";
var DOMParser = require('xmldom').DOMParser;

//http://streamplenty.streaming.mediaservices.windows.net/a36bfc1b-5d04-4303-81d8-47250b103554/b2e78d0aec874bcdb05cb319e0e54d1b.ism/QualityLevels(415269)/Manifest(video,format=m3u8-aapl)
//http://streamplenty.streaming.mediaservices.windows.net/bf7520c2-81ac-4174-b96d-296ce9b87866/8b0d9d6b831b4b8bbd7cf4e73a231ecd.ism/manifest(format=m3u8-aapl)

//http://streamplenty.streaming.mediaservices.windows.net/60920908-82f4-45c8-b6cd-676e25c9ff5a/dd8faa8980f9467e9d3482f85599dfaa.ism/manifest(format=m3u8-aapl)
export async function getVideoStream(accessToken) {
    try {
        console.log(types.GET_NEXT_VIDEO_REQUEST);
        let response = await fetch(types.GET_NEXT_VIDEO_REQUEST, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '676a4c0f-2dae-4079-96b1-73417485d74a',
            },
        });
        if (response.status >= 200 && response.status < 300) {
            let responseJson = await response.json();
            //console.log(responseJson.BaseUrl);
            let regexp = new RegExp('/.+?(?=manifest)/');
            let matcha = regexp.test(responseJson.BaseUrl);
            let baseUrl = matcha[0];
            response = await fetch(responseJson.BaseUrl, {
                method: 'GET',
            });
            let text = await response.text();
            //console.log(text);
            var doc = new DOMParser().parseFromString(text, 'text/xml');
            let bitratesArr = [];
            //console.log(doc);
            //let duration = doc.getAttribute('MajorVersion');

            //console.log(doc.getElementsByTagName('StreamIndex').getAttributes());
            let commoninfo = doc.getElementsByTagName('StreamIndex');
            //console.log('s' + bitratesArr);
            for (let i = 0; i < commoninfo[0].childNodes.length; i++) {
                if (commoninfo[0].childNodes[i].localName == 'QualityLevel') {
                    let btrt = commoninfo[0].childNodes[i].getAttribute('Bitrate');
                    let width = commoninfo[0].childNodes[i].getAttribute('MaxWidth');
                    let height = commoninfo[0].childNodes[i].getAttribute('MaxHeight');
                    //console.log(commoninfo[0].childNodes[i].localName);
                    bitratesArr.push({ bitrate: btrt, maxwidth: width, maxheigth: height });
                    //console.log(bitratesArr[i].bitrate);
                }
            }
            console.log('s1');
            return { newStreamUrl: matcha[0], bitrates: bitratesArr, duration: "duration" };
        }
        else {
            let errors = await response.json();
            throw errors;
        }
    } catch (errors) {
        throw errors;
    }
}
