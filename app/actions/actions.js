import * as types from '../constants/actionTypes';
import {getVideoStream} from '../services/api'

export function getNextVideo(accessToken) {
  return {
    type: types.GET_NEXT_VIDEO,
    payload: getVideoStream(accessToken)
  };
}

export function increment() {
  return {
    type: types.INCREMENT
  };
}

export function decrement() {
  return {
    type: types.DECREMENT
  };
}