import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Animated,
    Image,
    TouchableHighlight,
    TouchebleOpacity
} from 'react-native';

class Trigger extends Component {

    constructor(props) {
        super(props);

        this.state = {
            animationRadius: new Animated.Value(100),
            animationBorderRadius: new Animated.Value(100),
            animationTop: new Animated.Value(400),
            animationLeft: new Animated.Value(400),
        };
    }

    componentDidMount() {
        setInterval(this.toggle.bind(this), 410);
    }

    toggle() {
        Animated.sequence([
            Animated.timing(this.state.animationRadius, {
                toValue: 200,
                duration: 200,
            }),
            Animated.timing(this.state.animationRadius, {
                toValue: 100,
                duration: 200,
            }),
        ]).start();

        Animated.sequence([
            Animated.timing(this.state.animationTop, {
                toValue: this.props.topposition - 50,
                duration: 200,
            }),
            Animated.timing(this.state.animationTop, {
                toValue: this.props.topposition,
                duration: 200,
            }),
        ]).start();

        Animated.sequence([
            Animated.timing(this.state.animationLeft, {
                toValue: this.props.leftposition - 50,
                duration: 200,
            }),
            Animated.timing(this.state.animationLeft, {
                toValue: this.props.leftposition,
                duration: 200,
            }),
        ]).start();
    }

    onPressHandler() {
        this.props.onPress();
    }

    render() {
        return (

            <Animated.View
                style={{ height: this.state.animationRadius, width: this.state.animationRadius, borderRadius: this.state.animationBorderRadius, backgroundColor: 'red', position: 'absolute', top: this.state.animationTop, left: this.state.animationLeft }}>
                <TouchableHighlight onPress={() => this.onPressHandler() } style={{ flex: 1 }} underlayColor='transparent'>
                    <Text/>
                </TouchableHighlight>
            </Animated.View>

        );
    }
}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor: 'red'
    },
});

export default Trigger;